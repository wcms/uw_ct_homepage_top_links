<?php

/**
 * @file
 * uw_ct_homepage_top_links.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_homepage_top_links_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_homepage_top_links_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_homepage_top_links_node_info() {
  $items = array(
    'homepage_top_links' => array(
      'name' => t('Homepage Top Links'),
      'base' => 'node_content',
      'description' => t('For displaying top links on pathway pages (Future students, Current students, Faculty, Staff)'),
      'has_title' => '1',
      'title_label' => t('Title (admin use only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
