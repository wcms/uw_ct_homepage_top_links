<?php

/**
 * @file
 * uw_ct_homepage_top_links.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_homepage_top_links_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create homepage_top_links content'.
  $permissions['create homepage_top_links content'] = array(
    'name' => 'create homepage_top_links content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any homepage_top_links content'.
  $permissions['delete any homepage_top_links content'] = array(
    'name' => 'delete any homepage_top_links content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own homepage_top_links content'.
  $permissions['delete own homepage_top_links content'] = array(
    'name' => 'delete own homepage_top_links content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any homepage_top_links content'.
  $permissions['edit any homepage_top_links content'] = array(
    'name' => 'edit any homepage_top_links content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own homepage_top_links content'.
  $permissions['edit own homepage_top_links content'] = array(
    'name' => 'edit own homepage_top_links content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter homepage_top_links revision log entry'.
  $permissions['enter homepage_top_links revision log entry'] = array(
    'name' => 'enter homepage_top_links revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_top_links authored by option'.
  $permissions['override homepage_top_links authored by option'] = array(
    'name' => 'override homepage_top_links authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_top_links authored on option'.
  $permissions['override homepage_top_links authored on option'] = array(
    'name' => 'override homepage_top_links authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_top_links promote to front page option'.
  $permissions['override homepage_top_links promote to front page option'] = array(
    'name' => 'override homepage_top_links promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_top_links published option'.
  $permissions['override homepage_top_links published option'] = array(
    'name' => 'override homepage_top_links published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_top_links revision option'.
  $permissions['override homepage_top_links revision option'] = array(
    'name' => 'override homepage_top_links revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_top_links sticky option'.
  $permissions['override homepage_top_links sticky option'] = array(
    'name' => 'override homepage_top_links sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
